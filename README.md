# Api Control de inventario

Esta api permite gestionar el control de inventario

# Tecnologías


- DATABASE: MYSQL
- NODEJS
- TYPESCRIPT


## Instalación

`$ git clone url`


`$ npm i`


```Crear el .env segun el example.env```

```Ejecutar el script que se encuentra en el directorio /sql  - BD MYSQL```


```Configura en el archivo .env el puerto por donde iniciara la aplicacion, por defecto esta puerto 8000```

## Iniciar el proyecto

`$ npm start dev`

## Endpoint documentación

http://localhost:8000/api-doc


## Documentación POSTMAN

https://documenter.getpostman.com/view/7999344/2s83tCKt8M







