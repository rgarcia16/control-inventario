import {DataTypes} from 'sequelize';

import db from '../db/connection';

const Category = db.define('product_categories',{

    name:{
        type:DataTypes.STRING,
        allowNull: false

    },
    status:{
        type:DataTypes.BOOLEAN
    }
})


export default Category;