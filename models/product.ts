import {DataTypes} from 'sequelize';

import db from '../db/connection';

const Product = db.define('products',{

    name:{
        type:DataTypes.STRING,
        allowNull: false

    },
    category:{
        type:DataTypes.INTEGER,
        allowNull: false,
        references:{
            model: 'Category',
            key: 'id'
        }
    },
    description:{
        type:DataTypes.STRING,
        allowNull: false
    },
    quantity:{
        type:DataTypes.INTEGER,
        allowNull: false
    },
    userCreated: {
        field:"user_created",
        type: DataTypes.INTEGER,
        references: {
            model: 'User',
            key: 'id'
        },
    },
    price:{
        type:DataTypes.FLOAT,
        allowNull: false

    },
    available: { type: DataTypes.BOOLEAN,  defaultValue:true },
    
    status:{
        type:DataTypes.BOOLEAN,
        defaultValue:true
    },
    
})

export default Product;