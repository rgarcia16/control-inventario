import {DataTypes} from 'sequelize';

import db from '../db/connection';

const Role = db.define('roles',{

    name:{
        type:DataTypes.STRING,
        allowNull: false

    },
    status:{
        type:DataTypes.BOOLEAN,
        defaultValue:true
    }
})




export default Role;