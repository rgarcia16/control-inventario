import { DataTypes } from 'sequelize';

import db from '../db/connection';

const Purchase = db.define('purchases', {

    product: {
        type: DataTypes.INTEGER,
        allowNull: false

    },
    userBuyer: {
        field:"user_buyer",
        type: DataTypes.STRING,
        allowNull: false

    },
    quantity: {
        type: DataTypes.STRING,
        allowNull: false

    },
    price: {
        type: DataTypes.STRING,
        allowNull: false

    },

})

export default Purchase;