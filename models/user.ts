import {DataTypes} from 'sequelize';

import db from '../db/connection';

const User = db.define('users',{

    name:{
        type:DataTypes.STRING,
        allowNull: false

    },
    password:{
        type:DataTypes.STRING,
        allowNull: false

    },
    role:{
        type:DataTypes.STRING,
        allowNull: false,
        references: {
            model: 'Roles',
            key: 'id'
        },

    },
    email:{
        type:DataTypes.STRING,
        allowNull: false

    },
    status:{
        type:DataTypes.BOOLEAN,
        defaultValue:true
    },
  
})

export default User;