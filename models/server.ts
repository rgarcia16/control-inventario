
import express, { Application } from 'express';
import cors from 'cors'


//swagger
import swaggerUi from "swagger-ui-express";
import * as swaggerDocument from '../swagger/swagger.json'


import db from '../db/connection';
import userRoutes from '../routes/user'
import categoriaRoutes from '../routes/category'
import loginRoutes from '../routes/login'
import productosRoutes from '../routes/product'
import rolesRoute from '../routes/role'
import comprasRoute from '../routes/purchase'

class Server {

    private app: Application;
    private port: string;

    private apiPaths = {
        users: '/api/users',
        categories: '/api/categories',
        login: '/api/login',
        products: '/api/products',
        purchases: '/api/purchases',
        roles: '/api/roles',
        apiDoc: '/api-doc'

    }

    constructor() {
        this.app = express();
        this.port = process.env.PORT || '8000';

        //metodos iniciales
        this.conectarDB();
        this.middlewares()


        this.routes()
    }

    //conectar a bd 
    async conectarDB() {
        try {
            await db.authenticate();
            console.log('Database online')

        } catch (error) {

            console.log(error)

        }
    }


    routes() {
        this.app.use(this.apiPaths.users, userRoutes)
        this.app.use(this.apiPaths.categories, categoriaRoutes)
        this.app.use(this.apiPaths.login, loginRoutes)
        this.app.use(this.apiPaths.products, productosRoutes)
        this.app.use(this.apiPaths.roles, rolesRoute)
        this.app.use(this.apiPaths.purchases, comprasRoute)
        this.app.use(this.apiPaths.apiDoc, swaggerUi.serve,
            swaggerUi.setup(
                swaggerDocument
            ))

    }

    middlewares() {
        //cors
        this.app.use(cors());


        //lectura body
        this.app.use(express.json());

        //carpeta publica
        this.app.use(express.static('public'))

    }

    listen() {
        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto ' + this.port)
        })
    }
}

export default Server;

