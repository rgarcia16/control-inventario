
import { Request, Response } from "express"

type Next = () => void | Promise<void>;



const isAdminRole = ( req: Request, res: Response, next:Next ) =>{
  

   if (!req.body.userJWT){
        return res.status(400).json({
            msg:'Se quiere verificar el role sin validar el token primero'
        })
    }
    
    const {role, name} = req.body.userJWT
    if ( role !== 1){ //ADMIN ROL 
        return res.status(400).json({
            msg:` ${ name } no es administrador `
        })
    } 


    next();
}

const isUserRole = ( req: Request, res: Response, next:Next ) =>{
    


   if (!req.body.userJWT){
        return res.status(400).json({
            msg:'Se quiere verificar el role sin validar el token primero'
        })
    }
    
    const {role, name} = req.body.userJWT
    if ( role !== 2){
        return res.status(400).json({
            msg:` ${ name} no es user role `
        })
    } 


    next();
}

const haveRole = ( ...roles:string[] ) =>{


    return (req: Request, res:Response, next:Next) =>{
        
     const {usuario, rol } = req.body.userJWT


        if (!req.body.userJWT){
            return res.status(500).json({
                msg:'Se quiere verificar el role sin validar el token primero'
            })
        }

        if ( !roles.includes( req.body.userJWT.rol )){
            return res.status(500).json({
                msg:`El servicio requiere uno de estos valores ${roles}`
            })
            
        }

        next();
    }

}

export {isAdminRole, haveRole, isUserRole} 