import { Request, Response } from "express"

import jwt from 'jsonwebtoken';

import User from '../models/user'

type Next = () => void | Promise<void>;


const validateJWT = async ( req: Request, res : Response , next:Next) =>{
    
    const token = req.header('x-token')

    if(!token){
        return res.status(401).json({
            msg:"No hay token en la peticion"
        })
    }
   
    try {
       
        const { uid } = jwt.verify( token , process.env.SECRETORPRIVATEKEY || 'c0ntr0l1nv3nt4r10') as {
            uid: string;
        };

        //leer el usuario que corresponde al iid
        const user = await  User.findByPk(uid)

        if (!user){
            return res.status(401).json({
                msg:'Token no valido - usuario no existe en bd'
            })
        }

        //verficiar si el uid no esta eliminado logicamente
       if (!user.getDataValue('status')){
            return res.status(401).json({
                msg:'Token no valido - usuario con estado false'
            })
        }

        req.body.userJWT = user;
      
        next();
    } catch (error) {
        console.log(error)
        return res.status(401).json({
            msg:"Token no valido"
        })
        
    }

}

export {validateJWT}