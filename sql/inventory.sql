-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 04, 2022 at 06:45 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `category` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` text NOT NULL,
  `user_created` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `available` tinyint(4) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `category`, `quantity`, `description`, `user_created`, `status`, `available`, `createdAt`, `updatedAt`) VALUES
(1, 'BLUSAS', '100', 7, 4, 'blusas', 1, 1, 1, '2022-10-04 04:05:24', '2022-10-04 04:40:18'),
(2, 'TENNIS', '1500', 1, 5, 'zapatos deportivos', 1, 1, 1, '2022-10-04 04:15:55', '2022-10-04 04:15:55'),
(3, 'TENNIS1', '1500', 1, 5, 'zapatos deportivos', 1, 0, 1, '2022-10-04 04:16:47', '2022-10-04 04:21:54'),
(4, 'ZAPATOS', '1500', 1, 5, 'zapatos deportivos', 1, 0, 1, '2022-10-04 04:23:46', '2022-10-04 04:24:01'),
(5, 'CARDIGANS', '1200', 4, 100, 'cardigans', 1, 1, 1, '2022-10-04 04:42:42', '2022-10-04 04:42:42');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'CALZADOS.', 1, '2022-10-04 02:20:03', '2022-10-04 02:28:30'),
(2, 'JEANS', 1, '2022-10-04 02:27:45', '2022-10-04 02:29:17'),
(3, 'SWEATER', 1, '2022-10-04 02:27:55', '2022-10-04 02:27:55'),
(4, 'BLAZER', 0, '2022-10-04 02:28:09', '2022-10-04 02:28:58'),
(5, 'BUZOS', 1, '2022-10-04 02:28:15', '2022-10-04 02:28:15'),
(6, 'CHAQUETAS', 1, '2022-10-04 02:38:24', '2022-10-04 02:38:24'),
(7, 'T-SHIRTS', 0, '2022-10-04 03:37:30', '2022-10-04 03:40:04');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product` int(11) NOT NULL,
  `user_buyer` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `product`, `user_buyer`, `quantity`, `price`, `createdAt`, `updatedAt`) VALUES
(1, 1, 4, 4, 100, '2022-10-04 04:39:33', '2022-10-04 04:39:33'),
(2, 1, 4, 2, 100, '2022-10-04 04:40:18', '2022-10-04 04:40:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 'ADMIN_ROL', 1, '2022-10-03 19:44:45', '2022-10-03 19:44:45'),
(2, 'USER_ROL', 1, '2022-10-03 19:44:54', '2022-10-03 19:44:54'),
(3, 'VENTAS_ROL', 0, '2022-10-04 02:45:38', '2022-10-04 03:32:25'),
(6, 'VENTAS_ROL_1', 0, '2022-10-04 03:32:45', '2022-10-04 03:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `password`, `email`, `status`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Raquel Garcia R.', '$2a$10$4FGheHvo.XT5OCRZbLqn6O0PEzFytdurwLarif3XiPPvbrX99xfC2', 'raqgar16@gmail.com', 1, '2022-10-04 00:56:08', '2022-10-04 01:04:02'),
(2, 2, 'Raquel Garcia', '$2a$10$3vQViVNwG/jYz9WtBaVA9OP2gI5ffE0icJ/lTJDpjFwmzBrL5lEpS', 'raqgar17@gmail.com', 0, '2022-10-04 01:06:31', '2022-10-04 01:08:45'),
(3, 2, 'Avril Echeverria', '$2a$10$jvgcnwGUnXhk/KeY7lVJreQuBAhFD9p33Q30G72QpulY0x6SI9A1C', 'avril@gmail.com', 0, '2022-10-04 01:56:08', '2022-10-04 02:08:04'),
(4, 2, 'Avril Echeverria', '$2a$10$HRMCBeOQrdu6CaLzFQk8oeca.RMcKgBpznvD5XDegUV1u3mdyel1q', 'avril12@gmail.com', 1, '2022-10-04 02:51:38', '2022-10-04 02:51:38'),
(5, 1, 'Pedro Perez G', '$2a$10$wEx8Mh3P65WaR2e/Zfq.vup97chc8YTIU9m52Pn7yg4VKJvs0pqPK', 'test@gmail.com', 1, '2022-10-04 03:43:28', '2022-10-04 04:01:41'),
(6, 1, 'Pedro Perez', '$2a$10$g1jkjQ4SMAPxQ93chPs4puB4KfuhCycxQzs22OIxy7fqyu11uyceG', 'test@gmail.com', 0, '2022-10-04 03:55:50', '2022-10-04 03:57:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
