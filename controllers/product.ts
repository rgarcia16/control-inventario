
import { Category } from "@/models"
import { Request, Response } from "express"

import Product from "../models/product"

//Get Products 
export const getProducts = async (req: Request, res: Response) => {

    try {
        const productos  = await Product.findAll({
            attributes: ['id', 'name','quantity','price','category','description'],
            where: { status:true},
        
          })
        res.json({
            productos
        })
    } catch (error) {
        res.json({
            error
        })
    }

}

//get product by Id 
export const getProductById = async (req: Request, res: Response) => {

    const { productId } = req.params
    try {
        const producto = await Product.findByPk(productId)

        if (producto){
            res.json(producto)
        }else{
            res.status(404).json({
                msg:`No existe un producto con el ${productId}`
            })
        }
        
    } catch (error) {
        res.json({
            error
        })
    }
}

//borrar producto
export const deleteProduct = async (req: Request, res: Response) => {

    const { productId } = req.params;

    const producto = await Product.findByPk(productId)

    if (!producto){
        return  res.status(404).json({
            msg:'No existe un producto con el id '+ productId
        })
    }
   
    await producto.update({status:false})

    res.json(producto)
}

export const createProduct = async (req: Request, res: Response) => {


    try {
        const { status, user, ...resto} = req.body
    
        
        // Generar la data a guardar
        const data = {
            ...resto,
            name:req.body.name.toUpperCase(),
            userCreated: req.body.userJWT.id
        }
        const producto = await Product.create(data)
        
        res.status(201).json(
            producto
        )

        

    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg: 'error al crear el producto'
        }
        )
    }

}


export const updateProduct = async (req: Request, res: Response) => {

    const { productId } = req.params
    const {status, user, ... data} = req.body;

    if (data.name){
        data.name = data.name.toUpperCase()
    }
   
    data.usuario =  req.body.userJWT.id

    const producto = await Product.findByPk(productId)

    if (!producto){
        return  res.status(404).json({
            msg:'Ya existe un usuario con el id '+ productId
        })
    }
    

    await producto.update(data)

    res.json({
        producto,
    });
}
