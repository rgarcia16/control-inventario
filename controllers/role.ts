import { Request, Response } from "express"
import Role from '../models/role';


export const getRoles = async (req: Request, res: Response) => {

    try {
        const roles = await Role.findAll({
            attributes: ['id', 'name'],
            where: { status:true}
          })

        console.log(roles)
        res.json({
            roles,

        })
    } catch (error) {
        res.json({
            error
        })
    }
}

export const getRole= async (req: Request, res: Response) => {

    const { roleId } = req.params
    try {
        const role = await Role.findByPk(roleId)

        if (role){
            res.json(role)
        }else{
            res.status(404).json({
                msg:`No existe el rol con el ${roleId}`
            })
        }
        
    } catch (error) {
        res.json({
            error
        })
    }

}

export const createRol = async (req: Request, res: Response) => {

    const { body } = req;
    try {

        const existeRole = await Role.findOne({
            where:{
                name:body.name
            }
        })

        if (existeRole){
           return  res.status(404).json({
                msg:'Ya existe ese rol '+ body.name
            })
        }


        const role = await Role.create(body)
        await role.save()
        res.status(201).json(
            role
        )

        
        
    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg:'Hable con el administardor'
        })
    }


    
}
export const updateRole = async (req: Request, res: Response) => {

    const { body } = req;
    const { roleId } = req.params;
    try {

    
        const role = await Role.findByPk(roleId)

        if (!role){
            return  res.status(404).json({
                msg:'Ya existe una categoria con el id '+ roleId
            })
        }
        await role.update(body)
        

        res.json(role)

        
        
    } catch (error) {
        res.status(500).json({
            msg:'Consulte con el administardor'
        })
    }

}

export const deleteRole = async (req: Request, res: Response) => {

    const { roleId } = req.params;


    const role = await Role.findByPk(roleId)

    if (!role){
        return  res.status(404).json({
            msg:'Ya existe un usuario con el id '+ roleId
        })
    }
  
    await role.update({status:false})



    res.json(role)

}



