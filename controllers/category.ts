import { Request, Response } from "express"
import Category from '../models/category';



/** Get product categories */
export const getProductCategories = async (req: Request, res: Response) => {

    try {
        const categories = await Category.findAll({
            attributes: ['id', 'name'],
            where: { status:true}
          })

        console.log(categories)
        res.json({
            categories,

        })
    } catch (error) {
        res.json({
            error
        })
    }
}

export const getProductCategorie= async (req: Request, res: Response) => {

    const { categoryId } = req.params
    try {
        const category = await Category.findByPk(categoryId)

        if (category){
            res.json(category)
        }else{
            res.status(400).json({
                msg:`No existe la category con el ${categoryId}`
            })
        }
        
    } catch (error) {
        res.json({
            error
        })
    }

}

export const createProductCategory= async (req: Request, res: Response) => {

    const { body } = req;
    try {

        const existecategory = await Category.findOne({
            where:{
                name:body.name.toUpperCase()
            }
        })

        if (existecategory){
           return  res.status(404).json({
                msg:'Ya existe esa category '+ body.name
            })
        }

        body.name =req.body.name.toUpperCase()

        const category = await Category.create(body)
        await category.save()
        res.status(201).json(
            category
        )

        
        
    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg:'Hable con el administardor'
        })
    }


    
}
export const updateProductCategory = async (req: Request, res: Response) => {

    const { body } = req;
    const { categoryId } = req.params;
    try {

    
        const category = await Category.findByPk(categoryId)

        if (!category){
            return  res.status(404).json({
                msg:'No existe una categoria con el id '+ categoryId
            })
        }

        body.name =req.body.name.toUpperCase()

        await category.update(body)
        

        res.json(category)

        
        
    } catch (error) {
        res.status(500).json({
            msg:'Consulte con el administardor'
        })
    }

}

export const deleteProductCategory = async (req: Request, res: Response) => {

    const { categoryId } = req.params;

    const category = await Category.findByPk(categoryId)

    if (!category){
        return  res.status(404).json({
            msg:'Ya existe un usuario con el id '+ categoryId
        })
    }
  
    await category.update({status:false})



    res.json(category)

}



