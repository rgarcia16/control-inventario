import { Request, Response } from "express"
import bcryptjs from 'bcryptjs'
import User from '../models/user';
import { generateJWT } from '../helpers/generate-jwt'





export const login = async(req: Request, res: Response) => {

    const { email, password } = req.body
    try {

        //validate if exists email
        const user = await User.findOne({
            where:{
                email:email
            }
        })
        if (!user){
            return res.status(400).json({
                msg:'Usuario / password no son correctos - correo '
            })
        }

        //validate user status
        if (!user.getDataValue('status')){
            return res.status(400).json({
                msg:'Usuario / password no son correctos - estado:false '
            })
        }

        //validate password 
        const validarPassword = bcryptjs.compareSync(password, user.getDataValue('password'))
        if (!validarPassword){
            return res.status(400).json({
                msg:'Usuario / password no son correctos - password'
            })

        }

        //generate jwt 
        const token = await generateJWT( user.getDataValue('id'))

        res.json({
           user,
           token
        });

    } catch (error) {
        console.log(error)
        return res.status(500).json({
            msg:'Hable con el administrador'
        })

    }
  
}

