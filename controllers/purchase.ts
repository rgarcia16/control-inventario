import { Request, Response } from "express"
import Purchase from '../models/purchase'
import Product from "../models/product"
import { body } from "express-validator"


export const purchaseProduct = async (req: Request, res: Response) => {


    try {
        const { status, user, ...resto} = req.body
    
        const producto = await Product.findByPk(req.body.productId)

        if (!producto){
            return  res.status(404).json({
                msg:'No existe el producto '+ req.body.productId
            })
        }

        if (producto.getDataValue('quantity') == 0){
            return  res.status(404).json({
                msg:'No hay existencia del producto '+ req.body.name
            })
        }

        if (req.body.quantity > producto.getDataValue('quantity')){
            return  res.status(404).json({
                msg:'La cantidad solicitada es mayor a la existencia '+ req.body.name
            })
        }
        
        // Generar la data a guardar
        const data = {
            ...resto,
            product:req.body.productId,
            price:producto.getDataValue('price'),
            userBuyer: req.body.userJWT.id
        }
        const productoCreado = await Purchase.create(data)

        //descontar inventario
        
        const cantidadUp = {
            quantity:producto.getDataValue('quantity') - req.body.quantity
        }
        await producto.update(cantidadUp)
        
        res.status(201).json(
            productoCreado
        )

        

    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg: 'error al al crear la compra'
        }
        )
    }

}