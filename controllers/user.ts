import { Request, Response } from "express"
import bcryptjs from 'bcryptjs'

import User from '../models/user';
import  Role  from "../models/role";
import { roleExistsById } from "@/helpers";




export const getUsuarios = async (req: Request, res: Response) => {

    try {
        const users = await User.findAll( {where: { status:true}})
        res.json({
            users
        })
    } catch (error) {
        res.json({
            error
        })
    }

}

export const getUsuario = async (req: Request, res: Response) => {

    const { userId } = req.params
    try {
        const user = await User.findByPk(userId)

        if (user){
            res.json(user)
        }else{
            res.status(404).json({
                msg:`No existe un usuario con el ${userId}`
            })
        }
        
    } catch (error) {
        res.json({
            error
        })
    }

}

export const createUsuario = async (req: Request, res: Response) => {

    const { body } = req;

    try {

        const existeEmail = await User.findOne({
            where:{
                email:body.email
            }
        })

        if (existeEmail){
           return  res.status(404).json({
                msg:'Ya existe un usuario con el correo '+ body.email
            })
        }

        const role = await Role.findOne({
            where:{
                name:body.role
            }
        });
        if (!role) {
            throw new Error(`El  ${role} no existe `)
        } 

        //encriptar la contrasena
        const salt = bcryptjs.genSaltSync(10);
        body.password = bcryptjs.hashSync(body.password, salt);

        body.role =  role.getDataValue('id')
        const usuario = await User.create(body)

      //  await User.save()
        res.json(usuario)

        
        
    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg:'Contacte con el administardor'
        })
    }


    
}
export const updateUsuario = async (req: Request, res: Response) => {

    const { body } = req;
    const { userId } = req.params;
    try {

    
        const usuario = await User.findByPk(Number(userId))

        if (!usuario){
            return  res.status(404).json({
                msg:'Ya existe un usuario con el id '+ userId
            })
        }

        if (body.role){
            const role = await Role.findOne({
                where:{
                    name:body.role
                }
            });
            if (!role) {
                throw new Error(`El  ${role} no existe `)
            } 
    
            body.role =  role.getDataValue('id')
    
        }

        if (body.password){
             //encriptar la contrasena
             const salt = bcryptjs.genSaltSync(10);
             body.password = bcryptjs.hashSync(body.password, salt);

        }

    
         
        await usuario.update(body)
        

        res.json(usuario)

        
        
    } catch (error) {

        console.log(error)
        res.status(500).json({
            msg:'Contacte al administardor'
        })
    }

}

export const deleteUsuario = async (req: Request, res: Response) => {

    const { userId } = req.params;

    const usuario = await User.findByPk(userId)

    if (!usuario){
        return  res.status(404).json({
            msg:'Ya existe un usuario con el id '+ userId
        })
    }
    //eliminado fisico
    //await User.destroy();
    await usuario.update({status:false})



    res.json(usuario)

}



