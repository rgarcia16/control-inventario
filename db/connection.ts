import { Sequelize } from 'sequelize';



const username = process.env.USERNAME || 'root'
const database = process.env.DB || 'inventory'
const host = process.env.HOST || 'localhost'
const password = process.env.PASSWORD || ''

const db = new Sequelize( database, username, password, {
    host,
    dialect:'mysql',
});


export default db;
