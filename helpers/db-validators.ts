import { Category, Product } from '../models/index';
import Role from '../models/role.js';
import User from '../models/user.js'



export const isValidRole = async (role: string) => {
    const existeRol = await Role.findOne({
        where: {
            name: role
        }
    });
    if (!existeRol) {
        throw new Error(`El rol ${role} no esta registrado en BD `)
    }

}




export const emailExists = async (email: string) => {
    const existeEmail = await User.findOne({
        where: {
            email: email
        }
    })

    if (existeEmail) {
        throw new Error(`El correo ${email} ya esta registrado`)
    }

}

export const userExistsById = async (userId: number) => {
    const existeId = await User.findByPk(userId);
    if (!existeId) {
        throw new Error(`El  ${userId} no existe `)
    }

}

export const categoryExistsById = async (categoryId: number) => {


    const existeId = await Category.default.findByPk(categoryId);
    if (!existeId) {
        throw new Error(`El  ${categoryId} no existe `)
    }

}



export const categoryExistsByName = async (name: string) => {

    name = name.toUpperCase()

    const existeId = await Category.default.findOne({ where: { name: name } });
    if (existeId) {
        throw new Error(`La categoria ${name} ya existe `)
    }

}


export const roleExistsById = async (roleId: number) => {


    const existeId = await Role.findByPk(roleId);
    if (!existeId) {
        throw new Error(`El  ${roleId} no existe `)
    }

}

export const roleExistsByName = async (name: string) => {

    name = name.toUpperCase()

    const existeId = await Role.findOne({ where: { name: name } });
    if (existeId) {
        throw new Error(`El rol ${name} ya existe `)
    }

}

export const productExistsById = async (productId: number) => {

    const existeId = await Product.default.findByPk(productId);
    if (!existeId) {
        throw new Error(`El  ${productId} del producto no existe `)
    }

}

export const productExistsByName = async (name: string) => {


    name = name.toUpperCase()

    const existeId = await Product.default.findOne({ where: { name: name } });
    if (existeId) {
        throw new Error(`El producto ${name} ya existe `)
    }

}

export const allowedColeccions = (coleccion: string, colecciones: any[]) => {


    const incluida = colecciones.includes(coleccion);

    if (!incluida) {
        throw new Error(`La coleccion ${coleccion} no es permitida, ${colecciones}`)
    }

    return true
}









