import pkg from 'jsonwebtoken';

const generateJWT = (uid = '') =>{
    return new Promise( ( resolve, reject ) =>{
        const payload = {uid}

        const tokenPk = process.env.SECRETORPRIVATEKEY || 'c0ntr0l1nv3nt4r10'
        pkg.sign( payload, tokenPk , {
            expiresIn:'4h'
        }, (err, token) => {
            if (err) {
                console.log(err)
                reject('No se pudo generar el token ')
            }else{
                resolve( token )
            }
        })
    })
}

export {generateJWT}