

import { Router } from "express";
import { check } from 'express-validator';
import { roleExistsById, roleExistsByName} from '../helpers/db-validators';
import { validateFields } from '../middlewares/validate-fields';
import { validateJWT } from '../middlewares/validate-jwt';
import { isAdminRole } from '../middlewares/validate-role';

import { getRole, getRoles, createRol, deleteRole, updateRole } from '../controllers/role';

const router = Router()




router.get('/', getRoles)


/*

get category of product by id 
*/

router.get('/:roleId', [
    check('roleId', 'No es un id valido').isInt(),
    check('roleId').custom(roleExistsById),
    validateFields], getRole)


/*

create category of product
*/
router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields,
], createRol)


/*

update category of product

*/
router.put('/:roleId', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('roleId').custom(roleExistsById),
    check('name').custom(roleExistsByName),
    validateFields
], updateRole)


/*

delete category of product

*/

router.delete('/:roleId', [
    validateJWT,
    isAdminRole,
    check('roleId', 'No es un id valido').isInt(),
    check('roleId').custom(roleExistsById),
    validateFields], deleteRole)


export default router;