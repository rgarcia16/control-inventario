import { Router } from 'express';
import { check } from 'express-validator';
import { purchaseProduct } from '../controllers/purchase'
import { productExistsById } from '../helpers';
import { validateFields } from '../middlewares/validate-fields';
import { validateJWT } from '../middlewares/validate-jwt';
import { isUserRole } from '../middlewares/validate-role';

const router = Router()

router.post('/', [
    validateJWT,
    isUserRole,
    check('productId', 'El nombre es obligatorio').not().isEmpty(),
    check('quantity', 'La cantidad es obligatorio').not().isEmpty(),
    check('productId').custom(productExistsById),
    validateFields,
], purchaseProduct)
export default router;