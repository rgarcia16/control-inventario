import { Router } from 'express';
import { check } from 'express-validator';
import { login } from '../controllers/login'
import { validateFields } from '../middlewares/validate-fields.js';

const router = Router()

router.post('/', [
    check('email', 'El correo es obligatorio').isEmail(),
    check('password', 'El password es obligatorio').not().isEmpty(),    
    validateFields
], login)

export default router;