import { Router } from 'express';
import { check } from 'express-validator';

import {
    updateProduct, createProduct, deleteProduct,
    getProductById, getProducts
} from '../controllers/product.js';
import { categoryExistsById, productExistsById, productExistsByName, roleExistsByName } from '../helpers/db-validators.js';
import { validateFields } from '../middlewares/index.js';
import { validateJWT } from '../middlewares/validate-jwt.js';
import { isAdminRole } from '../middlewares/validate-role.js';
const router = Router()




//get all products 
router.get('/', getProducts)


//get product by id
router.get('/:productId', [
    check('productId', 'No es un id valido').isInt(),
    check('productId').custom(productExistsById),
    validateFields,
], getProductById)


//create product
router.post('/', [
    validateJWT,
    isAdminRole,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('price', 'El precio es obligatorio').not().isEmpty(),
    check('category', 'La categoria es obligatorio').not().isEmpty(),
    check('quantity', 'La cantidad es obligatorio').not().isEmpty(),
    check('quantity', 'La cantidad es obligatorio').isNumeric(),
    check('category', 'No es un id de categoria valido').isInt(),
    check('name').custom(productExistsByName),
    check('category').custom(categoryExistsById),
validateFields], createProduct)


//update product
router.put('/:productId', [
    validateJWT,
    check('productId', 'No es un id valido').isInt(),
    check('productId').custom(productExistsById),
    validateFields
], updateProduct)


//delete product
router.delete('/:productId',
    [
        validateJWT,
        isAdminRole,
        check('productId', 'No es un id valido').isInt(),
        check('productId').custom(productExistsById),
        validateFields], deleteProduct)


export default router;