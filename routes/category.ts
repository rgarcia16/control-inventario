

import { Router } from "express";
import { check } from 'express-validator';
import { categoryExistsById, categoryExistsByName } from '../helpers/db-validators';
import { validateFields } from '../middlewares/validate-fields';
import { validateJWT } from '../middlewares/validate-jwt';
import { isAdminRole } from '../middlewares/validate-role';

import { getProductCategorie, getProductCategories, createProductCategory,
     updateProductCategory, deleteProductCategory } from '../controllers/category';

const router = Router()


router.get('/', getProductCategories)


/*

Get Category by Id
*/

router.get('/:categoryId', [
    check('categoryId', 'No es un id valido').isInt(),
    check('categoryId').custom(categoryExistsById),
    validateFields], getProductCategorie)


/*
Create Category of products

*/
router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields,
], createProductCategory)


/*

Update Category of products

*/
router.put('/:categoryId', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('categoryId').custom(categoryExistsById),
    check('name').custom(categoryExistsByName),
    validateFields
], updateProductCategory)


/*

Delete Category of products

*/

router.delete('/:categoryId', [
    validateJWT,
    isAdminRole,
    check('categoryId', 'No es un id valido').isInt(),
    check('categoryId').custom(categoryExistsById),
    validateFields], deleteProductCategory)


export default router;