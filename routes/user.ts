

import { Router } from "express";
import { getUsuarios, getUsuario, createUsuario, updateUsuario, deleteUsuario } from '../controllers/user';
import { validateFields, validateJWT, haveRole , isAdminRole } from "../middlewares";
import { check } from 'express-validator';
import { isValidRole, emailExists, userExistsById } from '../helpers/db-validators';



const router = Router()

router.get('/', getUsuarios)


router.get('/:userId', [
    check('userId', 'No es un id valido').isInt(),
], getUsuario)

validateFields

router.post('/', [
    check('email', 'El correo no es valido').isEmail(),
    check('email').custom(emailExists),
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('password', 'El password debe ser mas de 6 letras').isLength({ min: 6 }),
    check('role').custom(isValidRole),
    validateFields
], createUsuario)



router.put('/:userId', [
    check('userId', 'No es un id valido').isInt(),
    check('userId').custom(userExistsById),
    validateFields
], updateUsuario)


router.delete('/:userId', [
    validateJWT,
    isAdminRole,
    check('userId', 'No es un id valido').isInt(),
    check('userId').custom(userExistsById),
    validateFields], deleteUsuario)


export default router;